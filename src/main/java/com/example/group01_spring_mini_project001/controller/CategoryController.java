package com.example.group01_spring_mini_project001.controller;

import com.example.group01_spring_mini_project001.model.entity.Category;
import com.example.group01_spring_mini_project001.model.request.CategoryRequest;
import com.example.group01_spring_mini_project001.model.response.CategoryResponse;
import com.example.group01_spring_mini_project001.service.AuthService;
import com.example.group01_spring_mini_project001.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/api/v1/categories")
@SecurityRequirement(name = "bearerAuth")

public class CategoryController {

    private final CategoryService categoryService;

    private final AuthService authService;
    public CategoryController(CategoryService categoryService, AuthService authService) {
        this.categoryService = categoryService;
        this.authService = authService;
    }


    // 1 - Get all categories
    @GetMapping("")
    @Operation(summary = "Get all categories")
    public ResponseEntity<CategoryResponse<List<Category>>> getAllCategory(){
        CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .payload(categoryService.getAllCategory())
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    // 2 - Get category by ID
    @GetMapping("/{id}")
    @Operation(summary = "Get category by id")
    public ResponseEntity<CategoryResponse<Category>> getCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<Category> response = null;

        response= CategoryResponse.<Category>builder()
                .payload(categoryService.getCategoryById(categoryId))
                .date((new Timestamp(System.currentTimeMillis())))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    // 3 - Add new category
    @PostMapping("/users")
    @Operation(summary = "Add new category")
    public ResponseEntity<CategoryResponse<Category>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        CategoryResponse<Category> response = null;

        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);

        Integer storeCategoryId = categoryService.addNewCategory(categoryRequest,userId);
        if(storeCategoryId != null){
            response = CategoryResponse.<Category>builder()
                    .payload(categoryService.getCategoryById(storeCategoryId))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    // 4 - Update Category by ID
    @PutMapping("/{id}/users")
    @Operation(summary = "Update category by Id")
    public ResponseEntity<CategoryResponse<Category>> updateCategory(@PathVariable("id") Integer categoryId, @RequestBody CategoryRequest categoryRequest){

        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);

        Category category = categoryService.updateCategoryById(categoryRequest, categoryId, userId);
        Integer storeId = category.getCategoryId();
        if (storeId != null){
            CategoryResponse response = CategoryResponse.<Category>builder()
                    .payload(categoryService.getCategoryById(categoryId))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    // 5 - get Category by ID for current user
    @GetMapping("/{id}/users")
    @Operation(summary = "Get category by ID for current user")
    public ResponseEntity<CategoryResponse<Category>> getCurrentUserCategoryById(@PathVariable("id") Integer categoryId){
        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);

        Category category = categoryService.getCurrentUserCategoryById(categoryId, userId);
        Integer storeId = category.getCategoryId();
        if (storeId != null){
            CategoryResponse response = CategoryResponse.<Category>builder()
                    .payload(categoryService.getCategoryById(categoryId))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    // 6 - get all Categories for current user
    @GetMapping("/users")
    @Operation(summary = "Get all Categories for current user")
    public ResponseEntity<CategoryResponse<List<Category>>> getCurrentUserAllCategories(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "false") Boolean asc,
            @RequestParam(defaultValue = "false") Boolean desc
    ){

        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);

        CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .payload(categoryService.getCurrentUserAllCategories(userId, page, size, asc, desc))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }



    // 7 - delete category by id for current user
    @DeleteMapping("/{id}/users")
    @Operation(summary = "Delete category by id for current user")
    public ResponseEntity<CategoryResponse<String>> deleteCategoryById(@PathVariable("id") Integer categoryId){
        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);

        if (categoryService.deleteCategoryById(categoryId, userId) == true){
            CategoryResponse response = CategoryResponse.<String>builder()
                    .payload("Delete this Category ID : " + categoryId + " is successful")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
