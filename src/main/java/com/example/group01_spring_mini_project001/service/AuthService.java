package com.example.group01_spring_mini_project001.service;

import com.example.group01_spring_mini_project001.model.entity.AppUser;
import com.example.group01_spring_mini_project001.model.entity.Auth;
import com.example.group01_spring_mini_project001.model.request.AuthAutheticationRequest;


public interface AuthService {
    Integer  addNewUser(AuthAutheticationRequest authAutheticationRequest);

    public AppUser getUserById(Integer userId);

    Integer getUserIdByEmail(String email);

}
