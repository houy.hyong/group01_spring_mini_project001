package com.example.group01_spring_mini_project001.service;

import java.util.List;

import com.example.group01_spring_mini_project001.model.entity.Category;
import com.example.group01_spring_mini_project001.model.request.CategoryRequest;

public interface CategoryService {


    // 1 - Get all categories
    List<Category> getAllCategory();


    // 2 - Get category by ID
    Category getCategoryById(Integer categoryId);


    // 3 - Add new category
    Integer addNewCategory(CategoryRequest categoryRequest, Integer userId);


    // 4 - Update Category by ID
    Category updateCategoryById(CategoryRequest categoryRequest, Integer categoryId, Integer userId);


    // 5 - get Category by ID for current user
    Category getCurrentUserCategoryById(Integer categoryId, Integer userId);


    // 6 - get all Categories for current user
    List<Category> getCurrentUserAllCategories(Integer userId, Integer page, Integer size, Boolean asc, Boolean desc);


    // 7 - delete category by id for current user
    boolean deleteCategoryById(Integer categoryId, Integer userId);
}
