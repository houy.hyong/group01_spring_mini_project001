package com.example.group01_spring_mini_project001.service;

import com.example.group01_spring_mini_project001.exception.AuthNotFoundException;
import com.example.group01_spring_mini_project001.model.entity.LoginUser;
import com.example.group01_spring_mini_project001.model.request.AuthAutheticationRequest;
import com.example.group01_spring_mini_project001.model.response.AuthAuthenticationResponse;
import com.example.group01_spring_mini_project001.repository.AuthRepository;
import com.example.group01_spring_mini_project001.securityConfig.JwtAuthenticationFilter;
import com.example.group01_spring_mini_project001.securityConfig.JwtUtil;
import com.example.group01_spring_mini_project001.service.serviceImp.AuthServiceImp;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtUtil jwtUtil;
    private final AuthServiceImp authServiceImp;
    private final AuthRepository authRepository;
    private final AuthenticationManager authenticationManager;

    public LoginUser authenticate(AuthAutheticationRequest request){

            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getEmail(),
                            request.getPassword()
                    )
            );

        var user = authRepository.getUserByEmail(request.getEmail());
            System.out.println(user);
            var jwtToken = jwtUtil.generateToken(user);
            return new LoginUser(user.getId(), request.getEmail(),jwtToken);
    }
}
