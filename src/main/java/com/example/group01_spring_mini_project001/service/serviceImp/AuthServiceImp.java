package com.example.group01_spring_mini_project001.service.serviceImp;

import com.example.group01_spring_mini_project001.exception.AuthNotFoundException;
import com.example.group01_spring_mini_project001.exception.InvalidException;
import com.example.group01_spring_mini_project001.model.entity.AppUser;
import com.example.group01_spring_mini_project001.model.entity.Auth;
import com.example.group01_spring_mini_project001.model.request.AuthAutheticationRequest;
import com.example.group01_spring_mini_project001.repository.AuthRepository;
import com.example.group01_spring_mini_project001.service.AuthService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImp implements UserDetailsService , AuthService{

    private final AuthRepository authRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public AuthServiceImp(AuthRepository authRepository, BCryptPasswordEncoder passwordEncoder) {
        this.authRepository = authRepository;
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Auth  userAuth = authRepository.getUserByEmail(username);
        if(userAuth == null){
            throw new AuthNotFoundException("User not found");
        }
        System.out.println(userAuth);
        return userAuth;
    }

    @Override
    public Integer addNewUser(AuthAutheticationRequest authAutheticationRequest) {
        System.out.println(authAutheticationRequest.getEmail());
        if(authAutheticationRequest.getEmail().equalsIgnoreCase("null")){
            throw new InvalidException("Email Can't Null");
        }else if(authAutheticationRequest.getEmail().isEmpty()){
            throw new InvalidException("Email Can't Empty");
        }else if(authAutheticationRequest.getEmail().isBlank()){
            throw new InvalidException("Email Can't Blank");
        }

        authAutheticationRequest.setPassword(passwordEncoder.encode(authAutheticationRequest.getPassword()));
        return authRepository.addNewUser(authAutheticationRequest);
    }

    @Override
    public AppUser getUserById(Integer userId) {
        return authRepository.getUserById(userId);
    }

    @Override
    public Integer getUserIdByEmail(String email) {
        return authRepository.getUserIdByEmail(email);
    }
}
