package com.example.group01_spring_mini_project001.service.serviceImp;

import com.example.group01_spring_mini_project001.dto.TaskDTO;
import com.example.group01_spring_mini_project001.enumStatus.Status;
import com.example.group01_spring_mini_project001.exception.EmptyFieldOrInvalidException;
import com.example.group01_spring_mini_project001.exception.NotOwnerException;
import com.example.group01_spring_mini_project001.exception.NotFoundException;
import com.example.group01_spring_mini_project001.mapper.TaskMapper;
import com.example.group01_spring_mini_project001.model.entity.Task;
import com.example.group01_spring_mini_project001.model.request.TaskRequest;
import com.example.group01_spring_mini_project001.repository.CategoryRepository;
import com.example.group01_spring_mini_project001.repository.TaskRepository;
import com.example.group01_spring_mini_project001.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImp implements TaskService {

    private final TaskRepository taskRepository;

    private final CategoryRepository categoryRepository;


    // 1- Get All Task
    @Override
    public List<TaskDTO> getAllTask() {
        List<Task> taskList = new ArrayList<>();
        List<TaskDTO> taskDTOList = new ArrayList<>();
        taskList =taskRepository.getAllTask();
        taskDTOList = TaskMapper.INSTANCE.listToTaskDTO(taskList);
        return taskDTOList;
    }

    // 2- Get Task By Id
    @Override
    public TaskDTO getTaskById(Integer taskId) {
        Task task = taskRepository.getTaskById(taskId);
        if(task!=null){
            return TaskMapper.INSTANCE.toTaskDto(task);
        }else{
            throw new NotFoundException("Task ID : "+taskId+ " is not found");
        }
    }



    // 3- Delete Current Task By Id
    @Override
    public boolean deleteTaskById(Integer taskId, Integer userId) {
        Task task = taskRepository.getTaskById(taskId);


        if(task != null){
            Integer storeUserId = task.getUserId();
            if (storeUserId == userId) {
                if (taskRepository.deleteTaskById(taskId) == true) {
                    return true;
                }

            } else {
                throw new NotOwnerException("Task ID : " + taskId + " cannot delete, because you're not owner !!");
            }
            return false;
        }else{
            throw new NotFoundException("Task ID : "+ taskId + " not found");
        }
    }



    // 4- Add New Tasks
    @Override
    public Task addTask(TaskRequest taskRequest, Integer userId) {

            if(taskRequest.getTaskName().isEmpty()){
                throw new EmptyFieldOrInvalidException("taskName could not be empty");
            }else if(taskRequest.getDescription().isEmpty()){
                throw new EmptyFieldOrInvalidException("description could not be empty");
            }
            else{
                boolean isStatus = false;
                for(Status status : Status.values()){
                    if(taskRequest.getStatus().toLowerCase().equalsIgnoreCase(status.name())){
                        isStatus = true;
                        break;
                    }
                }
                if(!isStatus){
                    throw new EmptyFieldOrInvalidException("Invalid Status, is_in_progress, is_completed, is_in_review, and is_cancelled");
                }
                if(categoryRepository.getAllCategoryId(taskRequest.getCategoryId())){
                    return taskRepository.addTask(taskRequest,userId);
                }else{
                    throw new NotFoundException("Category ID : "+taskRequest.getCategoryId() + " not found" );
                }


            }
        }

    // 5- Update Task Current User
    @Override
    public Task updateTask(TaskRequest taskRequest, Integer id, Integer userId) {
        Task task = taskRepository.getTaskById(id);
        if(task != null){
            Integer storeUserId = task.getUserId();
            if(storeUserId == userId){
                if(taskRequest.getTaskName().isEmpty()){
                    throw new EmptyFieldOrInvalidException("taskName could not be empty");
                }else if(taskRequest.getDescription().isEmpty()){
                    throw new EmptyFieldOrInvalidException("description could not be empty");
                }
                else{
                    boolean checkStatus = false;
                    for(Status status : Status.values()){
                        if(taskRequest.getStatus().toLowerCase().equalsIgnoreCase(status.name())){
                            checkStatus = true;
                            break;
                        }
                    }
                    if(!checkStatus){
                        throw new EmptyFieldOrInvalidException("Invalid Status, is_in_progress, is_completed, is_review, and is_cancelled");
                    }
                    if(categoryRepository.getAllCategoryId(taskRequest.getCategoryId())){
                        return taskRepository.updateTask(taskRequest,id,userId);
                    }else{
                        throw new NotFoundException("Category ID : "+taskRequest.getCategoryId() + " not found, cannot update" );
                    }
                }
            }else {
                throw new NotOwnerException("You cannot update Task ID : "+id+" because you're not owner");
            }
        }else{
            throw new NotFoundException("Task ID : "+ id +" cannot update because Task ID : "+ id + " not found");
        }


    }


    // 6- Get Task By Id for Current User
    @Override
    public Task getTaskCurrentUser(Integer taskId, Integer userId) {
        Task task = taskRepository.getTaskById(taskId);
        if(task != null){
            Integer storeUserId = task.getUserId();

            if(storeUserId == userId){
                if(taskRepository.getTaskCurrentUser(taskId,userId) != null){
                    return taskRepository.getTaskCurrentUser(taskId,userId);
                }else{
                    throw new NotFoundException("Task ID : "+taskId+ "doesn't exist!!");
                }
            }else
                throw new NotOwnerException("You're not the owner of Task ID : "+taskId);
        }else {
            throw new NotFoundException("Task ID : "+taskId+ " not found");

        }

        }


    // 7- Get All Tasks for Current User
    @Override
    public List<TaskDTO> getPageTask(Integer page, Integer size, Boolean asc, Boolean desc, Integer userId) {
        List<TaskDTO> task= taskRepository.getPageTask(page,size,asc,desc,userId);
        if(task.size() > 0){
            return  task;
        }else {
            throw  new NotFoundException("Data not found");
        }
    }


    // 8- Filter Task By Status for Current User
    @Override
    public List<TaskDTO> filterStatus(String status, Integer userId) {
        List<TaskDTO>taskDTOSList = taskRepository.filterStatus(status,userId);
        if(taskDTOSList.size() >0){
            return taskDTOSList;
        }else {
            boolean isStatus = false;
            for(Status loop : Status.values()){
                if(status.equalsIgnoreCase(loop.name())){
                    isStatus = true;
                    break;
                }
            }
            if(!isStatus){
                throw new EmptyFieldOrInvalidException("Invalid Status, is_in_progress, is_completed, is_in_review, and is_cancelled");
            }else {
                throw new EmptyFieldOrInvalidException(" No data ");
            }

        }
    }
}
