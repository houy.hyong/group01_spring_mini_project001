package com.example.group01_spring_mini_project001.service;

import com.example.group01_spring_mini_project001.dto.TaskDTO;
import com.example.group01_spring_mini_project001.model.entity.Task;
import com.example.group01_spring_mini_project001.model.request.TaskRequest;

import java.util.List;

public interface TaskService {

    // 1- Get All Task
    List<TaskDTO> getAllTask();

    // 2- Get Task By Id
    TaskDTO getTaskById(Integer taskId);

    // 3- Delete Current Task By Id
    boolean deleteTaskById(Integer taskId, Integer userId);


    // 4- Add New Task
    Task addTask(TaskRequest taskRequest, Integer userId);


    // 5- Update Task Current User
    Task updateTask(TaskRequest taskRequest, Integer id, Integer userId);


    // 6- Get Task By Id for Current User
    Task getTaskCurrentUser(Integer taskId, Integer userId);


    // 7- Get All Tasks for Current User
    List<TaskDTO> getPageTask(Integer page, Integer size, Boolean asc, Boolean desc, Integer userId);


    // 8- Filter Task By Status for Current User
    List<TaskDTO>filterStatus(String status, Integer userId);
}
