package com.example.group01_spring_mini_project001.repository;

import com.example.group01_spring_mini_project001.dto.TaskDTO;
import com.example.group01_spring_mini_project001.model.entity.Category;
import com.example.group01_spring_mini_project001.model.entity.Task;
import com.example.group01_spring_mini_project001.model.request.TaskRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TaskRepository {


    // 1- Get All Task
    @Results(
            id = "TaskMapper",
            value = {
                    @Result(property = "taskId", column = "id"),
                    @Result(property = "taskName", column = "name"),
                    @Result(property = "userId", column = "user_id"),
                    @Result(property = "categoryId", column = "category_id")
            }
    )
    @Select("""
            SELECT * FROM task_tb
            LIMIT #{size} OFFSET #{size} * (#{page}-1)
            """)
    List<Task> getAllTask();


    @Select("""
            SELECT tt.id, tt.user_id FROM category_tb INNER JOIN task_tb tt on category_tb.id = tt.category_id WHERE tt.id =#{id};
            """)
    List<Category>getCategoryById(Integer id);


    // 2- Get Task By Id
    @ResultMap("TaskMapper")
    @Select("""
            SELECT * FROM task_tb WHERE id = #{taskId}
            """)
    Task getTaskById(Integer taskId);


    // 3- Delete Current Task By Id
    @Delete("DELETE FROM task_tb WHERE id = #{taskId}")
    boolean deleteTaskById(Integer taskId);


    // 4- Add New Task
    @ResultMap("TaskMapper")
    @Select("""
            INSERT INTO task_tb (name, description, date, status, user_id, category_id )
            VALUES(#{request.taskName}, #{request.description},#{request.date},#{request.status},#{userId}, #{request.categoryId} )
            RETURNING id
            """)
    Task addTask(@Param("request") TaskRequest TaskRequest, Integer userId);


    // 5- Update Task Current User
    @ResultMap("TaskMapper")
    @Select("""
            UPDATE task_tb SET 
            name = #{update.taskName}, description = #{update.description},
            date = #{update.date}, status = #{update.status}, user_id = #{userId},
            category_id = #{update.categoryId} WHERE id = #{id}
            RETURNING id
            """)
    Task updateTask(@Param("update") TaskRequest taskRequest, Integer id, Integer userId);


    // 6- Get Task By Id for Current User
    @ResultMap("TaskMapper")
    @Select("""
            SELECT * FROM task_tb WHERE id = #{id} AND user_id = #{userId};
            """)
    Task getTaskCurrentUser(Integer id, Integer userId);

    // 7- Get All Tasks for Current User
    @Select("""
            SELECT * FROM task_tb WHERE user_id = #{userId} ORDER BY CASE WHEN #{asc} THEN date END ASC, CASE WHEN #{desc} THEN date END DESC
            LIMIT #{size} OFFSET #{size} * (#{page}-1) 
            """)
    @ResultMap("TaskMapper")
    List<TaskDTO>getPageTask(Integer page, Integer size, Boolean asc, Boolean desc, Integer userId);


    // 8- Filter Task By Status for Current User
    @Select("SELECT * FROM task_tb WHERE status = #{status} AND user_id = #{userId}")
    @ResultMap("TaskMapper")
    List<TaskDTO>filterStatus(String status, Integer userId);

}
