package com.example.group01_spring_mini_project001.repository;

import com.example.group01_spring_mini_project001.model.entity.Category;
import com.example.group01_spring_mini_project001.model.entity.Task;
import com.example.group01_spring_mini_project001.model.request.TaskRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import com.example.group01_spring_mini_project001.model.entity.Category;
import com.example.group01_spring_mini_project001.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    // 1 - Get all categories
    @Select("SELECT * FROM category_tb")
    @Results(
            id = "cateMapper",
            value = {
                    @Result(property = "categoryId", column = "id"),
                    @Result(property = "categoryName", column = "name"),
                    @Result(property = "userId", column = "user_id")
            }
    )
    List<Category> findAllCategory();


    // 2 - Get category by ID
    @Select(" SELECT * FROM category_tb WHERE id = #{categoryId} ")
    @ResultMap("cateMapper")
    Category getCategoryById(Integer categoryId);


    // 3 - Add new category
    @Select("INSERT INTO category_tb (name,user_id) " +
            "VALUES ( #{request.categoryName}, #{userId}  ) " +
            "RETURNING id")
    Integer addNewCategory(@Param("request") CategoryRequest categoryRequest, Integer userId);


    // 4 - Update Category by ID
    @Select("UPDATE category_tb " +
            "SET name = #{update.categoryName} " +
            "WHERE id = #{categoryId} " +
            "RETURNING id")
    @ResultMap("cateMapper")
    Category updateCategory(@Param("update") CategoryRequest categoryRequest, Integer categoryId);


    // 5 - get Category by ID for current user
    @Select("SELECT * FROM category_tb WHERE id = #{categoryId} AND user_id = #{userId} ")
    @ResultMap("cateMapper")
    Category getCurrentUserCategoryById(Integer categoryId, Integer userId);


    // 6 - get all Categories for current user
    @Select("SELECT * FROM category_tb WHERE user_id = #{userId} " +
            "ORDER BY " +
            "CASE WHEN #{asc} THEN date END ASC, " +
            "CASE WHEN #{desc} THEN date END DESC " +
            "LIMIT #{size} OFFSET #{size} * (#{page}-1) ")
    @ResultMap("cateMapper")
    List<Category> getAllCategoriesForCurrentUser(Integer userId, Integer page, Integer size, Boolean asc, Boolean desc);


    // 7 - delete category by id for current user
    @Delete("DELETE FROM category_tb WHERE id = #{categoryId} ")
    boolean deleteCategoryById(Integer categoryId);

    @Select("SELECT EXISTS(SELECT 1 FROM category_tb where id = #{categoryId}) ")
    boolean getAllCategoryId(Integer categoryId);
}
