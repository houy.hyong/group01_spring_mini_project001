package com.example.group01_spring_mini_project001.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class AuthAuthenticationResponse<T> {
    private  T payload;
    private  boolean  httpStatus;
    private Timestamp timestamp;
}
