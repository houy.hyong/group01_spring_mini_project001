package com.example.group01_spring_mini_project001.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryResponse<T> {
//    private String message;
    private  T payload;
//    private HttpStatus httpStatus;
    private Timestamp date;
    private Boolean success;
}
