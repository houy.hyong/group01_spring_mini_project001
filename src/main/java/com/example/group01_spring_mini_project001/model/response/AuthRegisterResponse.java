package com.example.group01_spring_mini_project001.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonPropertyOrder({"payload","timestamp", "httpStatus"})
public class AuthRegisterResponse<T> {
    private  T payload;
    private  boolean  httpStatus;
    private Timestamp timestamp;
}
