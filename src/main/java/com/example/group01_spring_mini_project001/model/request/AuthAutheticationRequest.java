package com.example.group01_spring_mini_project001.model.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthAutheticationRequest {

    @Email(message = "Invalid Email")
    private  String email;

//    @Pattern(
//            regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]){4,}$",
////            regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
//            message = "password must contain atleast 1 uppercase, 1 lowercase, 1 special character and 1 digit "
//    )
    private  String password;

}
