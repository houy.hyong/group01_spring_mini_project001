package com.example.group01_spring_mini_project001.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskRequest {
    private String taskName;
    private String description;
    private Timestamp date = new Timestamp(System.currentTimeMillis());

    private String status;

//    private Integer userId;
    private Integer categoryId;
}
