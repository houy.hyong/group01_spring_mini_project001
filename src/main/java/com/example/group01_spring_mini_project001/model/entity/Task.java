package com.example.group01_spring_mini_project001.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task {
    private Integer taskId;
    private String taskName;
    private String description;
    private Timestamp date;
    private String status;
    private Integer userId;
    private Integer categoryId;
}
