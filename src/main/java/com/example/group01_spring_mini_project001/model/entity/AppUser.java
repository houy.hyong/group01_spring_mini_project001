package com.example.group01_spring_mini_project001.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class AppUser {
    private  Integer id;
    private  String  email;
}
