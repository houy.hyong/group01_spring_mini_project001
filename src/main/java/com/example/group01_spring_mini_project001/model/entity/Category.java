package com.example.group01_spring_mini_project001.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Category {
    private Integer categoryId;
    private String categoryName;
    private Integer userId;
    private Timestamp date;
}
