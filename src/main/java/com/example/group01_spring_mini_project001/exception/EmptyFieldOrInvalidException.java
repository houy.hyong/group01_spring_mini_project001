package com.example.group01_spring_mini_project001.exception;

public class EmptyFieldOrInvalidException extends RuntimeException{
    public EmptyFieldOrInvalidException(String message) {
        super(message);
    }
}
