package com.example.group01_spring_mini_project001.exception;

public class AuthNotFoundException  extends RuntimeException{
    public  AuthNotFoundException(String message){
        super(message);
    }
}
