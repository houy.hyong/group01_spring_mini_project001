CREATE DATABASE to_do_list;

CREATE TABLE IF NOT EXISTS user_tb(
    id SERIAL PRIMARY KEY,
    email VARCHAR(40) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS category_tb(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40) NOT NULL ,
    date TIMESTAMP DEFAULT now(),
    user_id INTEGER REFERENCES user_tb(id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS task_tb(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(40) NOT NULL ,
    description VARCHAR(255),
    date TIMESTAMP,
    status VARCHAR(20),
    user_id INTEGER REFERENCES user_tb(id) ON UPDATE CASCADE ON DELETE CASCADE,
    category_id INTEGER REFERENCES category_tb(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO user_tb (email, password, role)
    VALUES ('jame@gmail.com','$2y$10$NxBKw3H8DA5rE2p9xoYCrugh7RaU.8zQACtyvrqqKCHM20J9vK.ae','USER'),
            ('hyong@gmail.com','$2y$10$r0nYnMmMl1F55/B6mIKIkes3.ljpfB1y5y0VgndShB0ChgIyo3cTW','USER'),
            ('him@gmail.com','$2y$10$T6W1074TkOIq9yX9CSlsu.QhTZWsgaIGcXh1L8up2bB1V6mGcxHDa','USER');

ALTER TABLE user_tb
    ALTER role SET DEFAULT 'USER';


INSERT INTO category_tb (name, date, user_id)
    VALUES ('School Task','2023-03-26', 1),
           ('Home Task', '2023-03-26', 2),
           ('Home Task', '2023-03-26',1);

INSERT INTO task_tb (name, description, date, status, user_id, category_id)
    VALUES ('Reading Books', 'reading in Korean book', '2023-03-26', 'is_completed', 1, 1),
           ('Do Spring homework', 'reading in Korean book', '2023-03-26', 'is_in_progress', 2, 1),
           ('Cleaning house', 'clean the house', '2023-03-26', 'is_completed', 1, 2);

